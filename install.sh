#!/bin/bash

WELCOME_MESSAGE='
    ___                        _ _          ____
   /   |  ______________ _____/ (_)___ _   / __ )____  _  __
  / /| | / ___/ ___/ __ `/ __  / / __ `/  / __  / __ \| |/_/
 / ___ |/ /  / /__/ /_/ / /_/ / / /_/ /  / /_/ / /_/ />  <
/_/  |_/_/   \___/\__,_/\__,_/_/\__,_/  /_____/\____/_/|_|

'

reboot_webserver_helper() {
    service apache2 restart

    echo 'Rebooting your webserver'
}


# /*=========================================
# =            CORE / BASE STUFF            =
# =========================================*/
apt-get update
apt-get -y upgrade
apt-get install -y build-essential
apt-get install -y tcl
apt-get install -y software-properties-common
apt-get install -y dos2unix
apt-get install -y vim
apt-get install -y git

apt-get install software-properties-common
add-apt-repository ppa:ondrej/php -y

apt-get update
apt-get -y upgrade


# /*======================================
# =            INSTALL APACHE            =
# ======================================*/
apt-get -y install apache2

# Remove "html" and add public
mv /var/www/html /var/www/public

# Clean VHOST with full permissions
MY_WEB_CONFIG='<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/public
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    <Directory "/var/www/public">
        Options Indexes FollowSymLinks
        AllowOverride all
        Require all granted
    </Directory>
</VirtualHost>'
echo "$MY_WEB_CONFIG" | tee /etc/apache2/sites-available/000-default.conf

# Squash annoying FQDN warning
echo "ServerName arcadiabox" | tee /etc/apache2/conf-available/servername.conf
a2enconf servername

# Enabled missing h5bp modules (https://github.com/h5bp/server-configs-apache)
a2enmod expires
a2enmod headers
a2enmod include
a2enmod rewrite
a2enmod ssl

reboot_webserver_helper


# /*===================================
# =            INSTALL PHP            =
# ===================================*/
apt-get -y install php

# Make PHP and Apache friends
apt-get -y install libapache2-mod-php

# Add index.php to readable file types
MAKE_PHP_PRIORITY='<IfModule mod_dir.c>
    DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
</IfModule>'
echo "$MAKE_PHP_PRIORITY" | tee /etc/apache2/mods-enabled/dir.conf

reboot_webserver_helper


# /*===================================
# =            PHP MODULES            =
# ===================================*/

# Base Stuff
apt-get -y install php-common
apt-get -y install php-all-dev

# Common Useful Stuff
apt-get -y install php-bcmath
apt-get -y install php-bz2
apt-get -y install php-cgi
apt-get -y install php-cli
apt-get -y install php-fpm
apt-get -y install php-imap
apt-get -y install php-intl
apt-get -y install php-json
apt-get -y install php-mbstring
apt-get -y install php-mcrypt
apt-get -y install php-odbc
apt-get -y install php-pear
apt-get -y install php-pspell
apt-get -y install php-tidy
apt-get -y install php-xmlrpc
apt-get -y install php-zip

# Enchant
apt-get -y install libenchant-dev
apt-get -y install php-enchant

# LDAP
apt-get -y install ldap-utils
apt-get -y install php-ldap

# CURL
apt-get -y install curl
apt-get -y install php-curl

# GD
apt-get -y install libgd2-xpm-dev
apt-get -y install php-gd

# IMAGE MAGIC
apt-get -y install imagemagick
apt-get -y install php-imagick


# /*===========================================
# =            CUSTOM PHP SETTINGS            =
# ===========================================*/
PHP_USER_INI_PATH=/etc/php/8.1/apache2/conf.d/user.ini

echo 'display_startup_errors = On' | tee -a $PHP_USER_INI_PATH
echo 'display_errors = On' | tee -a $PHP_USER_INI_PATH
echo 'error_reporting = E_ALL' | tee -a $PHP_USER_INI_PATH
echo 'upload_max_filesize = 64M' | tee -a $PHP_USER_INI_PATH
echo 'post_max_size = 64M' | tee -a $PHP_USER_INI_PATH
reboot_webserver_helper

# Disable PHP Zend OPcache
echo 'opache.enable = 0' | tee -a $PHP_USER_INI_PATH

# Absolutely Force Zend OPcache off...
sed -i s,\;opcache.enable=0,opcache.enable=0,g /etc/php/8.1/apache2/php.ini
reboot_webserver_helper


# /*================================
# =            PHP UNIT            =
# ================================*/
wget https://phar.phpunit.de/phpunit-9.5.phar
chmod +x phpunit-9.5.phar
mv phpunit-9.5.phar /usr/local/bin/phpunit
reboot_webserver_helper


# /*=============================
# =            MYSQL            =
# =============================*/
debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get -y install mysql-server
mysqladmin -uroot -proot create arcadiabox
apt-get -y install php-mysql
reboot_webserver_helper


# /*================================
# =            COMPOSER            =
# ================================*/
EXPECTED_SIGNATURE=$(wget -q -O - https://composer.github.io/installer.sig)
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")
php composer-setup.php --quiet
rm composer-setup.php
mv composer.phar /usr/local/bin/composer
chmod 755 /usr/local/bin/composer


# /*==============================
# =            WP-CLI            =
# ==============================*/
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
mv wp-cli.phar /usr/local/bin/wp


# /*=======================================
# =            WELCOME MESSAGE            =
# =======================================*/

# Disable default messages by removing execute privilege
chmod -x /etc/update-motd.d/*

# Set the new message
echo "$WELCOME_MESSAGE" | tee /etc/motd


# /*================================
# =            CLEAN UP            =
# ================================*/

dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY

apt-get -y autoremove
apt-get -y clean

cat /dev/null > ~/.bash_history && history -c
