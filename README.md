# Arcadia Box Build

Current Version: `1.0.x`

Scripts used to build the base arcadia box

## Features

* Git
* Apache
* PHP 8.1
* PHPUnit
* MySQL
* Composer
* WP-CLI

## Getting started

1. To get started run `vagrant up`
2. Package for distribution `vagrant package`
